﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SQLite4Unity3d;

public class Foo {
	[NotNull, PrimaryKey, AutoIncrement]
	public int id { get; set; }
	public int count { get; set; }
	public string name { get; set; }
	public double measure { get; set; }
}

public class DatabaseDemo : MonoBehaviour {

	DatabaseController databaseController;

	void Start () {
		databaseController = new DatabaseController ("Test.db");
		var openFlags = SQLiteOpenFlags.Create | SQLiteOpenFlags.ReadWrite;
		databaseController.Load (openFlags, DatabaseDidLoad);
	}

	void DatabaseDidLoad (DatabaseController sender) {
		Test5 ();
		databaseController.Save ();
		Debug.Log ("Done");
	}
		
	void Test1 () {
		// Don't need to do anything - the database was created when it was attempted to be loaded
	}

	// Creates a table for "Foo"
	void Test2 () {
		databaseController.connection.CreateTable<Foo> ();
	}

	// Create a few entries for "Foo"
	void Test3 () {
		databaseController.connection.CreateTable<Foo> ();
		List<Foo> foos = new List<Foo> (3);
		for (int i = 1; i <= 3; ++i) {
			var foo = new Foo {
				count = UnityEngine.Random.Range(0, 100),
				name = string.Format("Foo {0}", i),
				measure = UnityEngine.Random.value
			};
			foos.Add (foo);
		}
		databaseController.connection.InsertAll (foos);
	}

	// Fetch an entry and modify it
	void Test4 () {
		var foo = FetchByID (2);

		// Read
		Debug.Log ("Current name: " + foo.name);

		// Or Write
		foo.name = "New Name";

		// Apply changes
		databaseController.connection.Update (foo);
	}

	// Remove an entry
	void Test5 () {
		var foo = FetchByID (3);
		databaseController.connection.Delete (foo);
	}

	Foo FetchByID (int id) {
		return databaseController.connection.Table<Foo> ()
			.Where (x => x.id == id)
			.FirstOrDefault ();
	}
}