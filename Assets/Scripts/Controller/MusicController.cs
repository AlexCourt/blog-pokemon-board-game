﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {
	[SerializeField] AudioSource currentMusic;
	[SerializeField] AudioSource introMusic;
	[SerializeField] List<AudioSource> journeyMusic;
	[SerializeField] List<AudioSource> battleMusic;
	[SerializeField] List<AudioSource> victoryMusic;
	[SerializeField] AudioSource endMusic;
}