﻿using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State IntroState {
		get {
			if (_introState == null)
				_introState = new State(OnEnterIntroState, OnExitIntroState, "Intro");
			return _introState;
		}
	}
	State _introState;

	void OnEnterIntroState () {
		introLogoViewController.gameObject.SetActive (true);
		introTitleViewController.gameObject.SetActive (true);
		introTitleViewController.didFinish = delegate {
			stateMachine.ChangeState (SetupState);
		};
	}

	void OnExitIntroState () {
		introTitleViewController.didFinish = null;
		introTitleViewController.gameObject.SetActive (false);
	}
}