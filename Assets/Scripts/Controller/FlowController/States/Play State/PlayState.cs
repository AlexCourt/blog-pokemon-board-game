﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State PlayState {
		get {
			if (_playState == null)
				_playState = new State(OnEnterPlayState, null, "Play");
			return _playState;
		}
	}
	State _playState;

	void OnEnterPlayState () {
		stateMachine.ChangeState (GetReadyState);
	}
}