﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State NextPlayerState {
		get {
			if (_nextPlayerState == null)
				_nextPlayerState = new State(OnEnterNextPlayerState, null, "NextPlayer");
			return _nextPlayerState;
		}
	}
	State _nextPlayerState;

	void OnEnterNextPlayerState () {
		game.NextPlayer ();
		gameViewController.NextPlayer ();
		stateMachine.ChangeState (GetReadyState);
	}
}
