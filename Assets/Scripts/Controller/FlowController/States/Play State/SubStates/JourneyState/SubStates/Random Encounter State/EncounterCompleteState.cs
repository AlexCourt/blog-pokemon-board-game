﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State EncounterCompleteState {
		get {
			if (_encounterCompleteState == null)
				_encounterCompleteState = new State(OnEnterEncounterCompleteState, null, "EncounterComplete");
			return _encounterCompleteState;
		}
	}
	State _encounterCompleteState;

	void OnEnterEncounterCompleteState () {
		combatViewController.gameObject.SetActive(false);
		// TODO: Play Journey Music
		stateMachine.ChangeState (CheckDestinationState); // TODO: GymState
	}
}
