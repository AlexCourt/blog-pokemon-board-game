﻿using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State RandomEncounterState {
		get {
			if (_randomEncounterState == null)
				_randomEncounterState = new State(OnEnterRandomEncounterState, null, "RandomEncounter");
			return _randomEncounterState;
		}
	}
	State _randomEncounterState;

	void OnEnterRandomEncounterState () {

		Pokemon pokemon = RandomEncounterSystem.Apply(game.CurrentPlayer, board);
		if (pokemon == null) {
			stateMachine.ChangeState (CheckDestinationState); // TODO: GymState
			return;
		}

		randomEncounterViewController.Show (pokemon, delegate {
			randomEncounterViewController.didFinish = delegate(RandomEncounterViewController.Exits obj) {
				randomEncounterViewController.didFinish = null;
				randomEncounterViewController.Hide (delegate {
					switch (obj) {
					case RandomEncounterViewController.Exits.Capture:
						battle = BattleFactory.Create (game.CurrentPlayer, pokemon);
						stateMachine.ChangeState (StartEncounterBattleState);
						break;
					case RandomEncounterViewController.Exits.Ignore:
						stateMachine.ChangeState (CheckDestinationState); // TODO: GymState
						break;
					}
				});
			};
		});
	}
}
