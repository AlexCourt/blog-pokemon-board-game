﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {
	State GetSetState {
		get {
			if (_getSetState == null)
				_getSetState = new State(OnEnterGetSetState, null, "Get Set");
			return _getSetState;
		}
	}
	State _getSetState;

	void OnEnterGetSetState () {
		getSetViewController.Show (delegate {
			getSetViewController.didFinish = delegate(GetSetViewController.Exits obj) {
				getSetViewController.didFinish = null;
				getSetViewController.Hide (delegate {
					switch (obj) {
					case GetSetViewController.Exits.Roll:
						stateMachine.ChangeState (RollState);
						break;
					case GetSetViewController.Exits.Team:
						stateMachine.ChangeState (NextPlayerState); // TODO: TeamState
						break;
					}
				});
			};
		});
	}
}
