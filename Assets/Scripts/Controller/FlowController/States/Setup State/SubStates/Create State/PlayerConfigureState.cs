﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State PlayerConfigureState {
		get {
			if (_playerConfigureState == null)
				_playerConfigureState = new State(OnEnterPlayerConfigureState, OnExitPlayerConfigureState, "Player Configure");
			return _playerConfigureState;
		}
	}
	State _playerConfigureState;

	void OnEnterPlayerConfigureState () {
		playerConfigureViewController.gameObject.SetActive(true);
		playerConfigureViewController.didComplete = delegate {
			stateMachine.ChangeState(SetupCompleteState);
		};
		playerConfigureViewController.didAbort = delegate {
			stateMachine.ChangeState(PlayerCountState);
		};
	}

	void OnExitPlayerConfigureState () {
		playerConfigureViewController.didComplete = null;
		playerConfigureViewController.didAbort = null;
		playerConfigureViewController.gameObject.SetActive(false);
	}
}