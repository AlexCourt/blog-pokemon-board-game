﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using ECS;

public class TeamViewController : BaseViewController {

	[SerializeField] Image avatarImage;
	[SerializeField] Text speciesLabel;
	[SerializeField] Text cpLabel;
	[SerializeField] Text levelLabel;
	[SerializeField] Text healthLabel;
	[SerializeField] Text candiesLabel;
	[SerializeField] Text powerUpCostLabel;
	[SerializeField] Text evolutionCostLabel;
	[SerializeField] Text potionsLabel;
	[SerializeField] Text revivesLabel;
	[SerializeField] Text pageLabel;
	[SerializeField] Button powerUpButton;
	[SerializeField] Button evolveButton;
	[SerializeField] Button healButton;
	[SerializeField] Button reviveButton;
	[SerializeField] Button buddyButton;

	public void NextButtonPressed () {
		
	}

	public void PreviousButtonPressed () {
		
	}

	public void QuitButtonPressed () {
		
	}

	public void PowerUpButtonPressed () {
		
	}

	public void EvolveButtonPressed () {
		
	}

	public void HealButtonPressed () {
		
	}

	public void ReviveButtonPressed () {
		
	}

	public void BuddyButtonPressed () {
		
	}
}