﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class CaptureViewController : BaseViewController {

	public Action didComplete;

	[SerializeField] Sprite open;
	[SerializeField] Sprite closed;
	[SerializeField] Image pokeball;
	[SerializeField] Text resultLabel;

	Pokemon pokemon { get { return battle.combatants[1].CurrentPokemon; }}

	void OnEnable () {
		StartCoroutine(CaptureSequence());
	}

	IEnumerator CaptureSequence () {

		resultLabel.text = "";
		bool success = true;
		pokeball.sprite = open;
		yield return new WaitForSeconds(1);
		pokeball.sprite = closed;

		for (int i = 0; i < 3; ++i) {
			yield return StartCoroutine(Bounce());
			if (CaptureSystem.TryEscape(pokemon)) {
				success = false;
				break;
			}
		}

		resultLabel.text = success ? "Success!" : "Escaped!";
		yield return new WaitForSeconds(1);

		if (success)
			CaptureSystem.Capture (game.CurrentPlayer, pokemon);

		if (didComplete != null)
			didComplete();
	}

	IEnumerator Bounce () {
		RectTransform rt = pokeball.rectTransform;
		Tweener tweener = rt.ScaleTo( new Vector3(1.5f, 1.5f, 1.5f), 0.25f, EasingEquations.EaseOutCubic );
		while (tweener != null)
			yield return null;
		tweener = rt.ScaleTo(Vector3.one, 0.5f, EasingEquations.EaseOutBounce);
		while (tweener != null)
			yield return null;
	}
}