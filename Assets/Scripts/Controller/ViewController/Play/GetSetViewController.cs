﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GetSetViewController : BaseViewController {

	public enum Exits {
		Roll,
		Team
	}

	public Action<Exits> didFinish;

	[SerializeField] Text playerNameLabel;
	[SerializeField] Sprite emptyBadge;
	[SerializeField] Image[] badges;

	void OnEnable () {
		var player = game.CurrentPlayer;
		playerNameLabel.text = player.nickName;
		// TODO: Show earned badges
	}

	public void OnRollPressed () {
		if (didFinish != null)
			didFinish (Exits.Roll);
	}

	public void OnTeamPressed () {
		if (didFinish != null)
			didFinish (Exits.Team);
	}
}