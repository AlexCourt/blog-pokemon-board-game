﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AttackTeamViewController : BaseViewController {

	[SerializeField] AttackCandidateView[] candidateViews;
	[SerializeField] Button confirmButton;
	[SerializeField] Button cancelButton;
}