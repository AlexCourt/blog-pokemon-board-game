﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DialogViewController : BaseViewController {
	public Action didComplete;

	[SerializeField] Text titleLabel;
	[SerializeField] Text messageLabel;

	public void Show (string title, string message, Action didShow = null) {
		titleLabel.text = title;
		messageLabel.gameObject.SetActive (!string.IsNullOrEmpty (message));
		messageLabel.text = message;
		Show (didShow);
	}

	public void ContinueButtonPressed () {
		if (didComplete != null)
			didComplete();
	}
}