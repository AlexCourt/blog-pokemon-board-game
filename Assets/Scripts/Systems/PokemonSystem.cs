﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;
using SQLite4Unity3d;

public static class PokemonSystem {

	public static void SetLevel (this Pokemon pokemon, int level) {
		pokemon.level = level;
		float start = (float)pokemon.Stats.stamina / 2f;
		float stop = pokemon.Stats.stamina;
		float value = EasingEquations.Linear (start, stop, pokemon.LevelRatio);
		pokemon.maxHitPoints = Mathf.RoundToInt(value);
	}

	public static void SetEntity (this Pokemon pokemon, Entity entity) {
		pokemon.Name = entity.label;
		pokemon.entityID = entity.id;
		pokemon.Entity = entity;
		pokemon.Stats = entity.GetSpeciesStats();
		pokemon.Evolvable = entity.GetEvolvable();
	}

	public static void SetMoves (this Pokemon pokemon) {
		List<Move> moves = pokemon.Entity.GetMoves();
		List<Move> fastMoves = new List<Move>();
		List<Move> chargeMoves = new List<Move>();

		foreach (Move move in moves) {
			if (move.energy > 0)
				fastMoves.Add(move);
			else
				chargeMoves.Add(move);
		}

		if (fastMoves.Count == 0 || chargeMoves.Count == 0) {
			Debug.LogError("Missing moves");
			return;
		}

		pokemon.FastMove = fastMoves[Random.Range(0, fastMoves.Count)];
		pokemon.ChargeMove = chargeMoves[Random.Range(0, chargeMoves.Count)];

		pokemon.fastMoveID = pokemon.FastMove.id;
		pokemon.chargeMoveID = pokemon.ChargeMove.id;
	}

	public static void Restore (this Pokemon pokemon) {
		var connection = DataController.instance.pokemonDatabase.connection;
		var entity = connection.Table<Entity> ()
			.Where (x => x.id == pokemon.entityID)
			.FirstOrDefault();
		pokemon.SetEntity (entity);
		pokemon.FastMove = connection.Table<Move> ()
			.Where (x => x.id == pokemon.fastMoveID)
			.FirstOrDefault();
		pokemon.ChargeMove = connection.Table<Move> ()
			.Where (x => x.id == pokemon.chargeMoveID)
			.FirstOrDefault();
	}

	public static Sprite GetAvatar (this Pokemon pokemon, Poses pose = Poses.Front) {
		return GetAvatar(pokemon.Entity, pokemon.gender, pose);
	}

	public static Sprite GetAvatar (Entity entity, Genders gender, Poses pose) {
		string file = entity.id.ToString("000");
		string folder = pose == Poses.Front ? "PokemonFronts" : "PokemonBacks";
		string fileName = string.Format("{0}/{1}", folder, file);
		Sprite sprite = Resources.Load<Sprite>(fileName);
		if (sprite == null) {
			string extension = gender == Genders.Male ? "-M" : "-F";
			fileName = string.Format("{0}/{1}{2}", folder, file, extension);
			sprite = Resources.Load<Sprite>(fileName);
		}
		return sprite;
	}
}