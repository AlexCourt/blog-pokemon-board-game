﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameSystem {
	public static void NextPlayer (this Game game) {
		var index = (game.currentPlayerIndex + 1) % game.players.Count;
		game.currentPlayerIndex = index;
	}
}