﻿using SQLite4Unity3d;
using System.Collections.Generic;

namespace ECS {
	public class Type {
		[PrimaryKey, AutoIncrement]
		public int id { get; set; }
		public string name { get; set; }
	}

	public static class TypeExtensions {
		public static Type GetPrimaryType (this SpeciesStats stats) {
			return GetType(stats, stats.typeA);
		}

		public static Type GetSecondaryType (this SpeciesStats stats) {
			return GetType(stats, stats.typeB);
		}

		static Type GetType (SpeciesStats stats, int id) {
			var connection = DataController.instance.pokemonDatabase.connection;
			var match = connection.Table<Type>()
				.Where(x => x.id == id)
				.FirstOrDefault();
			return match;
		}

		public static List<SpeciesStats> StatsWithType (this Type type) {
			var connection = DataController.instance.pokemonDatabase.connection;
			var matches = connection.Table<SpeciesStats>()
				.Where(x => x.typeA == type.id || x.typeB == type.id);
			var result = new List<SpeciesStats>(matches);
			return result;
		}
	}
}
