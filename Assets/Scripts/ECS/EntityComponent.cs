﻿using SQLite4Unity3d;

namespace ECS {
	public class EntityComponent {
		[PrimaryKey, AutoIncrement]
		public int id { get; set; }
		public int entity_id { get; set; }
		public int component_id { get; set; }
		public int component_data_id { get; set; }
	}
}