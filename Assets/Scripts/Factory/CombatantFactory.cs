﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CombatantFactory {
	public static Combatant Create (Player player) {
		Combatant retValue = new Combatant();
		retValue.pokemon.Add(player.pokemon[0]);
		retValue.mode = ControlModes.Player;
		retValue.waitTime = retValue.CurrentPokemon.FastMove.duration;
		return retValue;
	}

	public static Combatant Create (Pokemon pokemon) {
		Combatant retValue = new Combatant();
		retValue.pokemon.Add(pokemon);
		retValue.mode = ControlModes.Computer;
		retValue.waitTime = pokemon.FastMove.duration;
		return retValue;
	}
}
