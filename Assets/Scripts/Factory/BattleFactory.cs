﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BattleFactory {
	public static Battle Create (Player player, Pokemon wildPokemon) {
		Battle retValue = new Battle ();
		retValue.combatants.Add (CombatantFactory.Create(player));
		retValue.combatants.Add (CombatantFactory.Create(wildPokemon));
		return retValue;
	}
}