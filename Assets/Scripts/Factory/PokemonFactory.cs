﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public static class PokemonFactory {

	public static Pokemon Create (Entity entity, int level = 0) {
		Pokemon pokemon = new Pokemon();
		pokemon.gender = UnityEngine.Random.Range(0, 2) == 0 ? Genders.Male : Genders.Female;
		pokemon.attackIV = UnityEngine.Random.Range(0, 16);
		pokemon.defenseIV = UnityEngine.Random.Range(0, 16);
		pokemon.staminaIV = UnityEngine.Random.Range(0, 16);
		pokemon.SetEntity (entity);
		pokemon.SetMoves ();
		pokemon.SetLevel (level);
		pokemon.hitPoints = pokemon.maxHitPoints;
		return pokemon;
	}
}