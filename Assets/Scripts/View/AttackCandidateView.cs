﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class AttackCandidateView : MonoBehaviour {

	public Text nameLabel;
	public Text cpLabel;
	public Text hpLabel;
	public Image avatarImage;
	public Button previousButton;
	public Button nextButton;
}