﻿using System.Collections.Generic;
using UnityEngine;
using ECS;

public class Combatant {
	public List<Pokemon> pokemon = new List<Pokemon>();
	public ControlModes mode;
	public double waitTime;
	public int currentPokemonIndex;
	public Pokemon CurrentPokemon { get { return pokemon[currentPokemonIndex]; }}
}